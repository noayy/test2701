import { StudentsService } from './../students.service';
import { AuthService } from './../auth.service';
import { Student } from './../interfaces/student';
import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.css']
})
export class StudentsComponent implements OnInit {

  students:Student[];
  displayedColumns: string[] = ['mathGrade','psychoGrade', 'pay', 'result','userId','delete'];
  students$;
  constructor(public authService:AuthService,private studentsService:StudentsService) { }

  delete(index){
    this.studentsService.delete(this.students[index].id); 
  }

  ngOnInit() {
        this.students$ = this.studentsService.getStudents(); 
        this.students$.subscribe(
          docs =>{
            this.students = [];
            for(let document of docs){
              const student:Student = document.payload.doc.data();
              student.id = document.payload.doc.id; 
              this.students.push(student); 
            }
          }

        ) 
      }

    
}
