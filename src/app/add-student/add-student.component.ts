import { AuthService } from './../auth.service';
import { Student } from './../interfaces/student';
import { StudentsService } from './../students.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PredictService } from '../predict.service';

@Component({
  selector: 'app-add-student',
  templateUrl: './add-student.component.html',
  styleUrls: ['./add-student.component.css']
})
export class AddStudentComponent implements OnInit {

  mathGrage;
  psychoGrage;
  pay = true;
  result;
  button=true;
  userId;
  selected;
  errMassage:string;
  err:boolean = false;


  constructor(private studentService:StudentsService,
              private predictService:PredictService,
              private router:Router,
              public auth:AuthService) { }

    cancel(){
      this.button = true;
      this.result = null;
    }

    save(sutdent:Student){
        this.studentService.addstudent(this.mathGrage,this.psychoGrage, this.pay, this.result,this.userId); 
        this.router.navigate(['/students']); 

    }

    predict(){
      if(this.mathGrage > 100 || this.mathGrage < 0 || this.psychoGrage >800 || this.psychoGrage < 0){
        this.err= true;
        this.errMassage = "please enter vaild grade";
        console.log(this.errMassage);
      }
      else{
      this.err= false;
      this.button = false;
      this.predictService.predict(this.mathGrage, this.psychoGrage, this.pay).subscribe(
        res=>{
          console.log(res);
          if (res > 0.5){
            var result = "will stay"
            this.selected = "will stay";
          } else{
            var result = "will leave"
            this.selected = "will leave";
          }
          this.result = result
        }
        );  
      }
      }
    
  ngOnInit(): void {
    this.auth.getUser().subscribe(
      user => {
        this.userId = user.email;
        console.log(this.userId); 
      }
      )  
  }

}
