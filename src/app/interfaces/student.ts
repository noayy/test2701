export interface Student {
    mathGrade:number,
    psychoGrage:number,
    pay:boolean,
    result?:string,
    userId?:string,
    id:string,
    created?
}
