import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PredictService {

  private url = "https://h6y751qynf.execute-api.us-east-1.amazonaws.com/beta2"

  predict(mathGrage:number, psychoGrage:number,pay:boolean):Observable<any>{
    let json = {'data':
      {'mathGrage':mathGrage,
      'psychoGrage':psychoGrage,
      'pay':pay}
    }
    let body = JSON.stringify(json);
    return this.http.post<any>(this.url,body).pipe(
      map(res => {
        console.log(res);
        let final = res.body;
        console.log(final); 
        return final;
      })
    );
  }

  constructor(private http:HttpClient) { }
}
