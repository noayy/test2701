import { Injectable } from '@angular/core';
import {AngularFirestore, AngularFirestoreCollection} from '@angular/fire/firestore';
import { Timestamp } from 'rxjs/internal/operators/timestamp';

@Injectable({
  providedIn: 'root'
})
export class StudentsService {

  studentsCollection:AngularFirestoreCollection = this.db.collection('students'); 

  addstudent(mathGrage:number,psychoGrage:number, pay:boolean, result:string, userId:string){
    const student = {mathGrade:mathGrage, psychoGrage:psychoGrage, pay:pay, result:result, userId:userId}; 
    this.studentsCollection.add(student);
  }


  
  public getStudents(){
    this.studentsCollection = this.db.collection(`students`,
    ref => ref); 
    return this.studentsCollection.snapshotChanges()
  }

  delete(id:string){
    this.db.doc(`students/${id}`).delete(); 
  } 

  constructor(private db:AngularFirestore) { }
}
