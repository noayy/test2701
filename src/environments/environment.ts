// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyAamSVrASYT3zLD5T78X28o91q1sDYnnPw",
    authDomain: "test2701-9b780.firebaseapp.com",
    projectId: "test2701-9b780",
    storageBucket: "test2701-9b780.appspot.com",
    messagingSenderId: "652431552263",
    appId: "1:652431552263:web:1d3636d62214645ac19fa2"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
